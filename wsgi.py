#!/usr/bin/python
import os

virtenv = os.path.join(os.environ.get('OPENSHIFT_PYTHON_DIR', '.'), 'virtenv')

try:
    execfile(virtenv, dict(__file__=virtenv))
except IOError:
    pass

#
# IMPORTANT: Put any additional includes below this line.  If placed above this
# line, it's possible required libraries won't be in your searchable path
#

from flaskapp import app as application


#
# Below for testing only
#
if __name__ == '__main__':
    application.run(host='0.0.0.0', port=8000, debug=True)
#    from wsgiref.simple_server import make_server
#    httpd = make_server('', 8000, application)
#    # Wait for a single request, serve it and quit.
#    httpd.serve_forever()
