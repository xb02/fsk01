from flask import render_template
from flaskapp import app

@app.route('/')
@app.route('/index')
def index():
    user = {'nickname': 'xbalaji'}  # fake user
    posts = [ 
        { 'author': {'nickname': 'anirudh'}, 'body': 'I know everything' },
        { 'author': {'nickname': 'balaji '}, 'body': 'what is the truth' }
    ]
    return render_template('index.html', title='fsk01', user=user, posts=posts)
